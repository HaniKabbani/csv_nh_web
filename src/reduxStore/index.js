import axios from "axios";
import thunk from "redux-thunk";
import { applyMiddleware, createStore, compose, combineReducers } from "redux";

import employeeReducer from "./reducers/employeesReducer";

const reducer = combineReducers({
  employees: employeeReducer,
});

axios.defaults.baseURL = "http://localhost:3001/employees/";

const store = createStore(reducer, compose(applyMiddleware(thunk)));

export default store;
export {
  getEmployees,
  failGetEmployees,
  updateEmployee,
  deleteEmployee,
  uploadFile
} from "./actions/employeesAction";
