import axios from "axios";
import { toast } from "react-toastify";

import { actionTypes } from "../actionTypes";

/**
 * dispatche inital action
 * @returns state updates action
 */
const initGetEmployees = () => {
  return {
    type: actionTypes.INIT_GET_EMPLOYEES,
    loading: true,
  };
};

/**
 * dispatche success action
 * @returns state updates action
 */
const successGetEmployees = (data) => {
  return {
    type: actionTypes.SUCCSESS_GET_EMPLOYEES,
    loading: false,
    employees: data,
    error: null,
  };
};

/**
 * dispatche fail action
 * @returns state updates action
 */
export const failGetEmployees = (error) => {
  return {
    type: actionTypes.FAIL_GET_EMPLOYEES,
    loading: false,
    employees: [],
    error: error,
  };
};

/**
 * fetch employees list
 * @param {boolean} skipInital
 * @returns dispatch action
 */
export const getEmployees = (skipInital = false) => {
  return (dispatch) => {
    if (!skipInital) dispatch(initGetEmployees());
    axios
      .get("")
      .then((res) => dispatch(successGetEmployees(res.data)))
      .catch((err) => dispatch(failGetEmployees(err.message)));
  };
};

/**
 * update employee data
 * @returns dispatch action
 */
export const updateEmployee = (data) => {
  return (dispatch) => {
    axios
      .put(`${data.id}`, data)
      .then((res) => {
        toast.success("Updated Succesfully");

        dispatch(getEmployees(true));
      })
      .catch((err) => {
        toast.error("Update Faild!");
        dispatch(failGetEmployees(err.message));
      });
  };
};

/**
 * delete employee data
 * @returns dispatch action
 */
export const deleteEmployee = (id) => {
  return (dispatch) => {
    axios
      .delete(`${id}`)
      .then((res) => {
        toast.success("Deleted Succesfully");
        dispatch(getEmployees(true));
      })
      .catch((err) => {
        toast.error("Delete Faild!");
        dispatch(failGetEmployees(err.message));
      });
  };
};
/**
 * delete employee data
 * @returns dispatch action
 */
export const uploadFile = (data) => {
  const formData = new FormData();
  data.forEach((file, i) => {
    formData.append(`file[${i}]`, file);
  });
  return (dispatch) => {
    axios
      .post(axios.defaults.baseURL.replace("employees", "uploads"))
      .then((res) => {
        toast.success("Files Uploaded Succesfully");
        dispatch(getEmployees(true));
      })
      .catch((err) => {
        toast.error("Files Upload Faild!");
        dispatch(failGetEmployees(err.message));
      });
  };
};
