import { actionTypes } from "../actionTypes";

const initState = {
  loading: false,
  sales: null,
  employees: [],
  error: null,
};

/**
 * initail state update
 * @param {object} state 
 * @param {object} action 
 * @returns  updated state
 */
const initGetDashboard = (state, action) => {
  return {
    ...state,
    loading: action.loading,
  };
};

/**
 * success state update
 * @param {object} state 
 * @param {object} action 
 * @returns  updated state
 */
const successGetDashboard = (state, action) => {
  return {
    ...state,
    loading: action.loading,
    employees: action.employees,
    error: action.error,
  };
};

/**
 * fail state update
 * @param {object} state 
 * @param {object} action 
 * @returns  updated state
 */
const failGetDashboard = (state, action) => {
  return {
    ...state,
    loading: action.loading,
    employees: action.employees,
    error: action.error,
  };
};

/**
 * employee reducer
 * @param {object} state 
 * @param {object} action 
 * @returns  updated employee state
 */
const Reducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.INIT_GET_EMPLOYEES:
      return initGetDashboard(state, action);
    case actionTypes.SUCCSESS_GET_EMPLOYEES:
      return successGetDashboard(state, action);
    case actionTypes.FAIL_GET_EMPLOYEES:
      return failGetDashboard(state, action);
    default:
      return { ...state };
  }
};

export default Reducer;
