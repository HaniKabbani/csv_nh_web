import { Container, Col, Row } from "react-bootstrap";
import { useState } from "react";
import { ToastContainer } from "react-toastify";

import { SideDrawer, SideMenu, UploadModal } from "./components/side";
import { Main } from "./components/main";

function App() {
  const [showUpload, setShowUpload] = useState(false);

  const closeUploadModal = () => {
    setShowUpload(false);
  };

  const openUploadModal = () => {
    setShowUpload(true);
  };

  return (
    <Container fluid className="h-100">
      <Row>
        <Col className="d-block d-md-none my-3">
          <SideDrawer openUploadModal={openUploadModal} />
        </Col>
      </Row>
      <Row className="h-100">
        <Col
          className="d-none d-md-block bg-dark-blue h-100 px-0"
          md="3"
          xl="2"
        >
          <SideMenu openUploadModal={openUploadModal} />
        </Col>
        <Col>
          <Main title={"Employees"} />
        </Col>
      </Row>
      <UploadModal
        showUpload={showUpload}
        closeUploadModal={closeUploadModal}
      />
      <ToastContainer
        position="bottom-left"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </Container>
  );
}

export default App;
