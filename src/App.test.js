import App from "./App";
import { render } from "./testUtils";

import "@testing-library/jest-dom";

test("it render the app", () => {
  render(<App />);
});
