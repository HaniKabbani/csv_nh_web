import { useState } from "react";
import { Button, CloseButton, Form, Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { uploadFile } from "../../../reduxStore";

export function UploadModal({ showUpload, closeUploadModal }) {
  const dispatch = useDispatch();
  const [files, setFiles] = useState([]);
  const [err, setErr] = useState("");

  const closeHandler = (e) => {
    closeUploadModal(false);
    setFiles([]);
    setErr("");
  };

  const changeHandler = (e) => {
    const uploadedFiles = e.target.files;
    const updateFiles = [];
    if (uploadedFiles) {
      setErr("");
      [...uploadedFiles].forEach((uploadeFile) => {
        if (uploadeFile.type === "text/csv")
          updateFiles.push({
            file: uploadeFile,
            valid: uploadeFile.size < 2 * Math.pow(10, 6),
          });
        else setErr("Please attach valid files format '.csv' extension");
      });
    }
    setFiles([...updateFiles]);
  };

  const uploadHandler = () => {
    if (files.length === 0) {
      setErr("Please attach files");
      return;
    }
    const invalide = files.filter((file) => file.valid === false);
    if (invalide.length !== 0) {
      setErr("Please attach valid files");
      return;
    }
    dispatch(uploadFile(files));
    setErr("");
    closeHandler()
  };

  return (
    <Modal
      show={showUpload}
      onHide={closeHandler}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      size="lg"
    >
      <Modal.Header className="px-5 d-flex flew-row">
        <CloseButton className="m-0" onClick={closeHandler} />
        <h4 className="text-center flex-grow-1">Upload File</h4>
      </Modal.Header>
      <Modal.Body className="px-5 pt-0">
        <Form.Group controlId="formFileMultiple" className="mt-5 mb-3">
          <Form.Label>Upload employees csv files</Form.Label>
          <Form.Control
            type="file"
            multiple
            accept=".csv"
            onChange={changeHandler}
          />
        </Form.Group>
        {files.length > 0 &&
          files.map((file, i) => (
            <div className="d-flex mb-2 " key={i}>
              <p className="fw=bold mb-0">
                {i + 1} - {file.file.name}
              </p>
              {!file.valid && (
                <span className="text-danger ms-2">
                  {"Invalid maximum file size is 2MB."}
                </span>
              )}
            </div>
          ))}
      </Modal.Body>

      <div className="px-5 my-4 d-grid gap-2">
        <Button
          variant="success"
          size="lg"
          type="submit"
          onClick={uploadHandler}
        >
          Upload
        </Button>
        <p className="text-center text-danger">{err}</p>
      </div>
    </Modal>
  );
}
