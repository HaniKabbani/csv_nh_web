import { render, screen, fireEvent } from "../../../testUtils";
import { SideMenu } from "./sideMenu";

test("it render the side menu", () => {
  const handleClick = jest.fn();
  const handleModal = jest.fn();

  render(<SideMenu handleClose={handleClick} openUploadModal={handleModal} />);

  const employeeEle = screen.getByText("Employees");
  const uploadEle = screen.getByText("Upload CSV");
  fireEvent.click(employeeEle);
  fireEvent.click(uploadEle);

  expect(employeeEle).toBeInTheDocument();
  expect(uploadEle).toBeInTheDocument();
  expect(handleClick).toBeCalledTimes(2);
  expect(handleModal).toBeCalledTimes(1);
  expect(
    screen.getByText("john don Doh Ab jack dooh dah third")
  ).toBeInTheDocument();
});
