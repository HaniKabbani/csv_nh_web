export function SideMenu({ handleClose, openUploadModal }) {
  const clickHander = () => {
    if (handleClose) handleClose();
  };

  return (
    <>
      <div className="text-center my-4">
        <img
          width="63px"
          height="63px"
          className="rounded-circle mr-3 mx-auto"
          src={"/blank.png"}
          alt=""
        />
      </div>
      <h5 className="text-truncate px-2 text-white my-5 text-capitalize text-center">
        john don Doh Ab jack dooh dah third
      </h5>
      <span
        role="link"
        variant="link"
        className="nav-item active cursor-pointer"
        onClick={clickHander}
      >
        Employees
      </span>
      <span
        role="link"
        variant="link"
        className="nav-item cursor-pointer"
        onClick={(e) => {
          openUploadModal();
          clickHander();
        }}
      >
        Upload CSV
      </span>
    </>
  );
}
