import { useState } from "react";
import { Button, Offcanvas } from "react-bootstrap";
import { HiOutlineMenuAlt2 } from "react-icons/hi";

import { SideMenu } from "../index";
export function SideDrawer({openUploadModal}) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        <HiOutlineMenuAlt2 className="fs-3 shadow-none" />
      </Button>

      <Offcanvas
        show={show}
        onHide={handleClose}
        className="bg-dark-blue text-white"
      >
        <Offcanvas.Header
          closeButton
          closeVariant="white"
          className="justify-content-end"
        />
        <Offcanvas.Body className="px-0">
          <SideMenu handleClose={handleClose} openUploadModal={openUploadModal} />
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}
