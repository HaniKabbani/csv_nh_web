import { CgSearchLoading } from "react-icons/cg";
import { FormControl } from "react-bootstrap";

export function Filter({ onMax, onMin, maxValue, minValue }) {
  return (
    <div className="w-100 my-5 d-flex flex-column flex-md-row">
      <div className="d-flex border border-1 flex-fill">
        <div className="d-none d-sm-block bg-secondary p-2">
          <CgSearchLoading className="fs-2" color="#3A3845" />
        </div>
        <div className="ms-1 ms-md-3 d-flex py-2 flex-fill">
          <small
            className="text-secondary fw-bold d-flex align-items-center d-lg-inline-block"
            style={{ minWidth: "max-content" }}
          >
            Minimum Salary
            <br />
            <small className="text-dark d-none d-lg-block">Enter Amount</small>
          </small>
          <span className="fs-4 ms-1 d-flex align-items-center">$</span>
          <FormControl
            className="border-0 py-0 flex-grow-1"
            type="number"
            onChange={onMin}
            value={minValue}
            min={0}
          />
        </div>
      </div>

      <span className="mx-2 py-2 d-none d-md-block">_</span>

      <div className="d-flex border border-1 mt-2 mt-md-0 flex-fill py-2">
        <div className="ms-1 ms-md-3 d-flex flex-fill">
          <small
            className="text-secondary fw-bold d-flex align-items-center d-lg-inline-block"
            style={{ minWidth: "max-content" }}
          >
            Maximiun Salary
            <br />
            <small className="text-dark d-none d-lg-block">Enter Amount</small>
          </small>
          <span className="fs-4 ms-1">$</span>
          <FormControl
            className="border-0 py-0  flex-grow-1"
            onChange={onMax}
            value={maxValue}
            type="number"
            min={10}
          />
        </div>
      </div>
    </div>
  );
}
