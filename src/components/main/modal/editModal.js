import * as Yup from "yup";
import { useDispatch } from "react-redux";
import {
  Button,
  CloseButton,
  FloatingLabel,
  Form,
  Modal,
} from "react-bootstrap";
import { useFormik } from "formik";
import { useEffect } from "react";
import { updateEmployee } from "../../../reduxStore";

export function EditModal({ show, employee, onHide }) {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      name: "",
      login: "",
      salary: "",
    },
    validationSchema: Yup.object().shape({
      name: Yup.string().required("Name is required"),
      login: Yup.string().required("Login is required"),
      salary: Yup.number()
        .required("Salary is required")
        .min(0, "Salary must be number and greater than 0"),
    }),
    onSubmit: (values) => {
      console.log(values);
      const data = {
        ...values,
        id: employee.id,
      };
      dispatch(updateEmployee(data));
      closeHandler();
    },
  });

  const { setValues, resetForm } = formik;

  const closeHandler = (e) => {
    resetForm();
    onHide({ show: "init", employee: null });
  };

  useEffect(() => {
    if (employee) {
      const { name, login, salary } = employee;
      setValues({ name, login, salary: salary.toFixed(2) });
    }
  }, [employee, setValues]);

  return (
    show === "edit" &&
    employee && (
      <Modal
        show={show === "edit"}
        onHide={closeHandler}
        aria-labelledby="contained-modal-title-vcenter"
        centered
        size="lg"
      >
        <Modal.Header className="px-5 d-flex flew-row">
          <CloseButton className="m-0" onClick={closeHandler} />
          <h4 className="text-center flex-grow-1">Edit</h4>
        </Modal.Header>
        <Modal.Body className="px-5 pt-0">
          <h5 className="my-4">Edit Employee id#{employee.id}</h5>
          <Form
            noValidate
            className="border border-1 rounded-2"
            onSubmit={formik.handleSubmit}
          >
            <FloatingLabel label="Name">
              <Form.Control
                className="border-0 border-bottom bg-light shadow-none"
                type="text"
                isInvalid={formik.touched.name && formik.errors.name}
                placeholder="Employee Name"
                {...formik.getFieldProps("name")}
              />
              <Form.Control.Feedback
                type="invalid"
                className="bg-light mt-0 px-2"
              >
                {formik.errors.name}
              </Form.Control.Feedback>
            </FloatingLabel>

            <FloatingLabel label="Login">
              <Form.Control
                className="border-0 border-bottom bg-light shadow-none"
                type="text"
                isInvalid={formik.touched.login && formik.errors.login}
                placeholder="Employee Login"
                {...formik.getFieldProps("login")}
              />
              <Form.Control.Feedback
                type="invalid"
                className="bg-light mt-0 px-2"
              >
                {formik.errors.login}
              </Form.Control.Feedback>
            </FloatingLabel>

            <FloatingLabel label="Salary">
              <Form.Control
                className="border-0 bg-light shadow-none"
                type="number"
                min={0}
                isInvalid={formik.touched.salary && formik.errors.salary}
                placeholder="Employee Salary"
                {...formik.getFieldProps("salary")}
              />
              <Form.Control.Feedback
                type="invalid"
                className="bg-light mt-0 px-2"
              >
                {formik.errors.salary}
              </Form.Control.Feedback>
            </FloatingLabel>
          </Form>
        </Modal.Body>

        <div className="px-5 my-4 d-grid gap-2">
          <Button
            variant="success"
            size="lg"
            type="submit"
            onClick={(e) => formik.submitForm()}
          >
            Save
          </Button>
        </div>
      </Modal>
    )
  );
}
