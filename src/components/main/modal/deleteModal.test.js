import { render, screen, fireEvent } from "../../../testUtils";
import { DeleteModal } from "./deleteModal";

test("it have correct employee", () => {
  const handleClick = jest.fn();

  render(
    <DeleteModal
      show="delete"
      employee={{ id: "e001", name: "test@1" }}
      onHide={handleClick}
    />
  );

  const title = screen.getByText("Delete Employee id#e001");
  const deleteQuestion = screen.getByText(
    "Do you want to delete employee test@1 ?"
  );
  const btnDelete = screen.getByText("Delete");
  btnDelete.click(title);

  expect(title).toBeInTheDocument();
  expect(deleteQuestion).toBeInTheDocument();
  expect(handleClick).toBeCalledTimes(1);
});
