import { Button, Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { deleteEmployee } from "../../../reduxStore";

export function DeleteModal({ show, employee, onHide }) {
  const dispatch = useDispatch();

  const deleteHandler = (id) => {
    dispatch(deleteEmployee(id));
    onHide({ show: "init", employee: null });
  };
  return (
    show === "delete" &&
    employee && (
      <Modal
        show={show === "delete"}
        onHide={(e) => onHide({ show: "delete", employee: null })}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Delete Employee id#{employee.id}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Do you want to delete employee {employee.name} ?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={(e) => onHide({ show: "init", employee: null })}>
            Close
          </Button>
          <Button variant="danger" onClick={(e) => deleteHandler(employee.id)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    )
  );
}
