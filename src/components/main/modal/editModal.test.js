import { render, screen } from "../../../testUtils";
import { EditModal } from "./editModal";

test("it have correct employee", () => {
  const handleClick = jest.fn();

  render(
    <EditModal
      show="edit"
      employee={{
        id: "e001",
        name: "test@1",
        login: "test@login",
        salary: 1200.343,
      }}
      onHide={handleClick}
    />
  );

  const title = screen.getByText("Edit Employee id#e001");
  const nameInput = screen.getByText("Name");
  const nameLogin = screen.getByText("Login");
  const nameSalary = screen.getByText("Salary");
  const btnSave = screen.getByText("Save");
  expect(nameInput).toBeInTheDocument();
  expect(nameLogin).toBeInTheDocument();
  expect(nameSalary).toBeInTheDocument();

  expect(screen.queryByPlaceholderText("Employee Name").value).toBe("test@1");
  expect(screen.queryByPlaceholderText("Employee Login").value).toBe(
    "test@login"
  );
  expect(screen.queryByPlaceholderText("Employee Salary").value).toBe(
    "1200.34"
  );

  expect(title).toBeInTheDocument();
  expect(btnSave).toBeInTheDocument();
});
