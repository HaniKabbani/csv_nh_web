import DataTable from "react-data-table-component";
import { Alert, Button } from "react-bootstrap";
import { MdEdit, MdDelete } from "react-icons/md";
import { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import customStyles from "./customStyles";
import { DeleteModal, EditModal, Filter } from "../index";
import { getEmployees, failGetEmployees } from "../../../reduxStore";

export const Table = () => {
  const dispatch = useDispatch();
  const { employees, loading, error } = useSelector((state) => state.employees);

  const [maxValue, setMax] = useState("");
  const [minValue, setMin] = useState("");
  const [employee, setEmplyee] = useState({
    // SHOW  VALUES: ["init", "edit", "delete"]
    show: "init",
    employee: null,
  });

  const emplyeeHandler = (state) => setEmplyee(state);

  const filteredEmployees = employees.filter((item) => {
    if (minValue === "" && maxValue === "") return true;
    else if (minValue !== "" && maxValue === "")
      return item.salary && Number(item.salary) > Number(minValue);
    else if (minValue === "" && maxValue !== "")
      return item.salary && Number(item.salary) < Number(maxValue);
    return (
      item.salary &&
      Number(item.salary) > Number(minValue) &&
      Number(item.salary) < Number(maxValue)
    );
  });

  const subHeader = useMemo(() => {
    return (
      <Filter
        onMax={(e) => setMax(e.target.value)}
        onMin={(e) => setMin(e.target.value)}
        maxValue={maxValue}
        minValue={minValue}
      />
    );
  }, [maxValue, minValue]);

  const columns = useMemo(
    () => [
      {
        name: "Id",
        selector: (row) => row.id,
        cell: (row) => (
          <div className="w-100 d-flex align-items-center">
            <img
              src="/Blank.png"
              alt="employee"
              width={20}
              height={20}
              className="d-none d-lg-inline-block mx-3"
            />
            <div className="text-center flex-fill me-0 me-lg-4">
              <span>{row.id}</span>
            </div>
          </div>
        ),
        center: true,
        sortable: true,
      },
      {
        name: "Name",
        selector: (row) => row.name,
        sortable: true,
      },
      {
        name: "Login",
        selector: (row) => row.login,
        sortable: true,
      },
      {
        name: "Salary",
        selector: (row) => row.salary,
        format: (row) => row.salary.toFixed(2),
        sortable: true,
      },
      {
        name: "Action",
        cell: (row) => (
          <>
            <MdEdit
              onClick={(e) => setEmplyee({ show: "edit", employee: row })}
              className="fs-3 me-2 text-primary cursor-pointer"
            />
            <MdDelete
              onClick={(e) => setEmplyee({ show: "delete", employee: row })}
              className="fs-3 text-danger cursor-pointer"
            />
          </>
        ),
        ignoreRowClick: true,
        button: true,
      },
    ],
    []
  );

  useEffect(() => {
    dispatch(getEmployees());
  }, [dispatch]);

  return (
    <>
      {error ? (
        <Alert variant="danger" className="my-5">
          <Alert.Heading>Oops.. an error occurred!</Alert.Heading>
          <p>{error}</p>
          <Button
            variant="danger"
            onClick={(e) => {
              dispatch(failGetEmployees(null));
              dispatch(getEmployees());
            }}
          >
            Reload
          </Button>
        </Alert>
      ) : (
        <DataTable
          columns={columns}
          data={filteredEmployees}
          pagination
          progressPending={loading}
          paginationRowsPerPageOptions={[5, 10, 15]}
          fixedHeader
          customStyles={customStyles}
          fixedHeaderScrollHeight={"640px"}
          paginationPerPage={5}
          subHeader
          subHeaderComponent={subHeader}
        />
      )}
      <DeleteModal
        show={employee.show}
        employee={employee.employee}
        onHide={emplyeeHandler}
      />
      <EditModal
        show={employee.show}
        employee={employee.employee}
        onHide={emplyeeHandler}
      />
    </>
  );
};
