module.exports = {
  rows: {
    style: {
      marginBottom: 20,
      border: "none",
      backgroundColor: "#EFEFEF ",
      borderRadius: "15px",
    },
  },
  subHeader: {
    style: {
      padding: 0,
    },
  },
  headCells: {
    style: {
      fontWeight: "800",
    },
  },
  cells: {
    style: {
      fontWeight: "500",
    },
  },
  pagination: {
    style: {
      border: "none",
    },
  },
};
