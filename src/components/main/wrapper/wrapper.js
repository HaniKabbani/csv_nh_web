import { Container } from "react-bootstrap";
import { Table } from "../index";

export const Main = ({ title }) => {
  return (
    <Container className="mt-5">
      <h1>{title}</h1>
      <Table />
    </Container>
  );
};
